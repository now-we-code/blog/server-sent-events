# Server-Sent Events

Example on how to use server-sent-events with etcd.\
Details on how to install and how to use in the blog article: [Envoi de données depuis le serveur vers le client avec les server sent events](https://www.nowwecode.com/blog/envoi-de-donnees-depuis-le-serveur-vers-le-client-avec-les-server-sent-events)
