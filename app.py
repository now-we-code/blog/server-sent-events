import etcd3
from flask import Flask, Response, render_template, request

app = Flask(__name__)
etcd = etcd3.client(host='etcd', port=2379)  # 127.0.0.1


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/stream')
def stream():
    return Response(eventStream(), mimetype='text/event-stream')


def eventStream():
    status, _ = etcd.get('/status')
    yield 'data: {}\n\n'.format(status.decode())

    events_iterator, _ = etcd.watch('/status')
    for event in events_iterator:
        yield 'data: {}\n\n'.format(event.value.decode())
